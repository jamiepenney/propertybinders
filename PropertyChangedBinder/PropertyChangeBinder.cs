﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Linq.Expressions;

namespace PropertyBinders
{
    public static class Binder
    {
        public static PropertyChangedBinder<TSource, TTarget> BindOnPropertyChanged<TSource, TTarget>()
            where TSource : INotifyPropertyChanged 
        {
            return new PropertyChangedBinder<TSource, TTarget>();
        }
    }

    public class PropertyChangedBinder<TSource, TTarget> 
        where TSource : INotifyPropertyChanged 
    {
        private readonly List<SinglePropertyChangedBinder> singleBinders
            = new List<SinglePropertyChangedBinder>();

        private readonly List<CollectionPropertyChangedBinder> collectionBinders
            = new List<CollectionPropertyChangedBinder>();

        public SinglePropertyChangedBinder<TSource, TSourceProperty, TTarget> BindProperty<TSourceProperty>(Expression<Func<TSource, TSourceProperty>> getProperty)
        {
            var binder = new SinglePropertyChangedBinder<TSource, TSourceProperty, TTarget>(this, getProperty);
            singleBinders.Add(binder);
            return binder;
        }

        public CollectionPropertyChangedBinder<TSource, TTarget, TCollectionModel, TCollectionTarget>
            BindCollection<TCollectionModel, TCollectionTarget>(Expression<Func<TSource, ObservableCollection<TCollectionModel>>> getProperty)
        {
            var binder = new CollectionPropertyChangedBinder<TSource,  TTarget, TCollectionModel, TCollectionTarget>(this, getProperty);
            collectionBinders.Add(binder);
            return binder;
        }

        public PropertyChangedBinding<TSource, TTarget> BindObjects(TSource model, TTarget viewModel)
        {
            return new PropertyChangedBinding<TSource, TTarget>(this, model, viewModel);
        }

        public void PropertyChanged(TSource source, TTarget target, string propertyName)
        {
            foreach (var binder in singleBinders.Where(b => b.Matches(propertyName)))
            {
                binder.Execute(source, target);
            }

            foreach (var binder in collectionBinders.Where(b => b.Matches(propertyName)))
            {
                binder.Sync((ICollectionPropertyChanged)source, target);
            }
        }

        public void Sync(TSource source, TTarget target)
        {
            foreach (var binder in singleBinders)
            {
                binder.Execute(source, target);
            }

            foreach (var binder in collectionBinders)
            {
                binder.Sync((ICollectionPropertyChanged)source, target);
            }
        }

        public void OnCollectionPropertyChanged(TSource source, TTarget target, CollectionPropertyChangedEventArgs args)
        {
            foreach (var binder in collectionBinders.Where(b => b.Matches(args.PropertyName)))
            {
                binder.Execute((ICollectionPropertyChanged) source, target, args);
            }
        }
    }

    public class PropertyChangedBinding<TSource, TTarget>
        where TSource : INotifyPropertyChanged
    {
        private readonly PropertyChangedBinder<TSource, TTarget> _binder;
        private readonly WeakReference _source;
        private readonly WeakReference _target;

        public PropertyChangedBinding(PropertyChangedBinder<TSource, TTarget> binder, TSource source, TTarget target)
        {
            _binder = binder;
            _source = new WeakReference(source);
            _target = new WeakReference(target);
            source.PropertyChanged += OnPropertyChanged;

            var cc = source as ICollectionPropertyChanged;
            if (cc != null)
            {
                cc.CollectionPropertyChanged += OnCollectionPropertyChanged;
            }
        }

        private void OnCollectionPropertyChanged(object sender, CollectionPropertyChangedEventArgs args)
        {
            if (_target.IsAlive == false)
            {
                DestoryListeners();
                return;
            }
            _binder.OnCollectionPropertyChanged((TSource)_source.Target, (TTarget)_target.Target, args);
        }

        private void OnPropertyChanged(object sender, PropertyChangedEventArgs args)
        {
            if (_target.IsAlive == false)
            {
                DestoryListeners();
                return;
            }

            _binder.PropertyChanged((TSource) _source.Target, (TTarget) _target.Target, args.PropertyName);
        }

        public void Sync()
        {
            _binder.Sync((TSource) _source.Target, (TTarget) _target.Target);
        }

        private void DestoryListeners()
        {
            if (_source.IsAlive == false) return;

            var source = ((TSource)_source.Target);

            source.PropertyChanged -= OnPropertyChanged;
            var cc = source as ICollectionPropertyChanged;
            if (cc != null)
            {
                cc.CollectionPropertyChanged += OnCollectionPropertyChanged;
            }
        }
    }

    public abstract class SinglePropertyChangedBinder
    {
        public abstract bool Matches(string propertyName);
        public abstract void Execute(INotifyPropertyChanged source, object target);
    }

    public abstract class CollectionPropertyChangedBinder
    {
        public abstract bool Matches(string propertyName);
        public abstract void Execute(ICollectionPropertyChanged source, object target, CollectionPropertyChangedEventArgs args);
        public abstract void Sync(ICollectionPropertyChanged sourceObject, object targetObject);
    }

    public static class LambdaHelpers
    {
        //involves recursion
        public static string GetMemberName(this LambdaExpression memberSelector)
        {
            Func<Expression, string> nameSelector = null;  //recursive func
            nameSelector = e => //or move the entire thing to a separate recursive method
            {
                switch (e.NodeType)
                {
                    case ExpressionType.Parameter:
                        return ((ParameterExpression)e).Name;
                    case ExpressionType.MemberAccess:
                        return ((MemberExpression)e).Member.Name;
                    case ExpressionType.Call:
                        return ((MethodCallExpression)e).Method.Name;
                    case ExpressionType.Convert:
                    case ExpressionType.ConvertChecked:
                        return nameSelector(((UnaryExpression)e).Operand);
                    case ExpressionType.Invoke:
                        return nameSelector(((InvocationExpression)e).Expression);
                    case ExpressionType.ArrayLength:
                        return "Length";
                    default:
                        throw new Exception("not a proper member selector");
                }
            };

            return nameSelector(memberSelector.Body);
        }
    }
}
