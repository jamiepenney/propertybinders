﻿using System;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Linq;
using System.Linq.Expressions;
using PropertyBinders.Extensions;

namespace PropertyBinders
{
    public class CollectionPropertyChangedBinder<TSource, TTarget, TCollectionModel, TCollectionTarget> : CollectionPropertyChangedBinder
        where TSource : INotifyPropertyChanged
    {
        private readonly PropertyChangedBinder<TSource, TTarget> _parent;
        private readonly string _getPropertyName;
        private readonly Func<TSource, ObservableCollection<TCollectionModel>> _getSourceFunc;
        private Func<TTarget, Collection<TCollectionTarget>> _getTargetFunc;
        private Func<TCollectionModel, TCollectionTarget> _convert;
        private Func<TTarget, TCollectionModel, TCollectionTarget> _find;

        public CollectionPropertyChangedBinder(PropertyChangedBinder<TSource, TTarget> parent, Expression<Func<TSource, ObservableCollection<TCollectionModel>>> getProperty)
        {
            _parent = parent;
            _getSourceFunc = getProperty.Compile();
            _getPropertyName = getProperty.GetMemberName();
        }

        public CollectionPropertyChangedBinder<TSource, TTarget, TCollectionModel, TCollectionTarget> 
            Converter(Func<TCollectionModel, TCollectionTarget> convert)
        {
            _convert = convert;
            return this;
        }

        public CollectionPropertyChangedBinder<TSource, TTarget, TCollectionModel, TCollectionTarget>
            Finder(Func<TTarget, TCollectionModel, TCollectionTarget> find)
        {
            _find = find;
            return this;
        }

        public PropertyChangedBinder<TSource, TTarget> To(Func<TTarget, Collection<TCollectionTarget>> getTargetFunc)
        {
            _getTargetFunc = getTargetFunc;
            return _parent;
        }

        public override bool Matches(string propertyName)
        {
            return propertyName == _getPropertyName;
        }

        public override void Execute(ICollectionPropertyChanged source, object targetObject, CollectionPropertyChangedEventArgs args)
        {
            var target = (TTarget)targetObject;
            var collection = _getTargetFunc(target);

            switch (args.Event.Action)
            {
                case NotifyCollectionChangedAction.Add:
                    foreach (var item in args.Event.NewItems.ReverseList<TCollectionModel>())
                    {
                        var newModel = _convert(item);
                        collection.Insert(args.Event.NewStartingIndex, newModel);
                    }
                    break;
                case NotifyCollectionChangedAction.Remove:
                    foreach (var item in args.Event.OldItems.Cast<TCollectionModel>())
                    {
                        var model = _find(target, item);
                        collection.Remove(model);
                    }
                    break;
                case NotifyCollectionChangedAction.Reset:
                    collection.Clear();
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        public override void Sync(ICollectionPropertyChanged sourceObject, object targetObject)
        {
            var source = _getSourceFunc((TSource) sourceObject);
            var target = _getTargetFunc((TTarget)targetObject);

            target.Clear();

            foreach (var item in source)
            {
                target.Add(_convert(item));
            }
        }
    }
}