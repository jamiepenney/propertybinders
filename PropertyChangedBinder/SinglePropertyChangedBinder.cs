﻿using System;
using System.ComponentModel;
using System.Linq.Expressions;

namespace PropertyBinders
{
    public class SinglePropertyChangedBinder<TSource, TSourceProperty, TTarget> : SinglePropertyChangedBinder
        where TSource : INotifyPropertyChanged 
    {
        private readonly PropertyChangedBinder<TSource, TTarget> _parent;
        private readonly Func<TSource, TSourceProperty> _getProperty;
        private readonly string _getPropertyName;
        private Action<TTarget, TSourceProperty> _setProperty;

        public SinglePropertyChangedBinder(PropertyChangedBinder<TSource, TTarget> parent, Expression<Func<TSource, TSourceProperty>> getProperty)
        {
            _parent = parent;
            _getProperty = getProperty.Compile();
            _getPropertyName = getProperty.GetMemberName();
        }

        public PropertyChangedBinder<TSource, TTarget> To(Action<TTarget, TSourceProperty> setValue)
        {
            _setProperty = setValue;
            return _parent;
        }

        public override bool Matches(string propertyName)
        {
            return propertyName == _getPropertyName;
        }

        public override void Execute(INotifyPropertyChanged source, object target)
        {
            var value = _getProperty.Invoke((TSource) source);
            _setProperty((TTarget)target, value);
        }
    }
}