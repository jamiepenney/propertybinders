﻿using System.Collections;
using System.Collections.Generic;

namespace PropertyBinders.Extensions
{
    public static class ListExtensions
    {
        public static IEnumerable<T> ReverseList<T>(this IList list)
        {
            for (int i = list.Count-1; i >= 0; i--)
            {
                yield return (T)list[i];
            }
        }
    }
}