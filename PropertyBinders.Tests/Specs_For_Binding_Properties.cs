﻿using System.ComponentModel;
using System.Runtime.CompilerServices;
using NUnit.Framework;
using PropertyBinders.Tests.Annotations;

namespace PropertyBinders.Tests
{
    [TestFixture]
    public class Specs_For_Binding_Properties
    {
        private Model model;
        private ViewModel viewModel;
        private PropertyChangedBinder<Model, ViewModel> binder;

        [Test]
        public void It_Should_Be_Able_To_Construct_A_Single_Property_Binder()
        {
            binder.BindObjects(model, viewModel);
            model.Name = "New Name";

            Assert.That(viewModel.Name, Is.EqualTo("New Name"));
        }

        [Test]
        public void It_Should_Be_Able_To_Sync_A_Single_Property_Binder()
        {
            model.Name = "Name1";
            binder.BindObjects(model, viewModel).Sync();

            Assert.That(viewModel.Name, Is.EqualTo("Name1"));
        }

        public class Model : INotifyPropertyChanged
        {
            private string _name;

            public string Name
            {
                get { return _name; }
                set
                {
                    if (value == _name) return;
                    _name = value;
                    OnPropertyChanged();
                }
            }

            public event PropertyChangedEventHandler PropertyChanged;

            [NotifyPropertyChangedInvocator]
            protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
            {
                var handler = PropertyChanged;
                if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        public class ViewModel : INotifyPropertyChanged
        {
            private string _name;

            public string Name
            {
                get { return _name; }
                set
                {
                    if (value == _name) return;
                    _name = value;
                    OnPropertyChanged();
                }
            }

            public event PropertyChangedEventHandler PropertyChanged;

            [NotifyPropertyChangedInvocator]
            protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
            {
                var handler = PropertyChanged;
                if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        [TestFixtureSetUp]
        public void Setup()
        {
            model = new Model();
            viewModel = new ViewModel();

            binder = Binder
                .BindOnPropertyChanged<Model, ViewModel>()
                .BindProperty(m => m.Name)
                .To((v, u) => v.Name = u);
        }
    }
}
