﻿using System;
using System.Collections.Specialized;
using System.ComponentModel;

namespace PropertyBinders
{
    public interface ICollectionPropertyChanged : INotifyPropertyChanged
    {
        event CollectionPropertyChangedEventHandler CollectionPropertyChanged;
    }

    public delegate void CollectionPropertyChangedEventHandler(object sender, CollectionPropertyChangedEventArgs e);

    public class CollectionPropertyChangedEventArgs : EventArgs
    {
        public CollectionPropertyChangedEventArgs(string propertyName, NotifyCollectionChangedEventArgs args)
        {
            PropertyName = propertyName;
            Event = args;
        }

        public string PropertyName { get; private set; }
        public NotifyCollectionChangedEventArgs Event { get; private set; }
    }
}