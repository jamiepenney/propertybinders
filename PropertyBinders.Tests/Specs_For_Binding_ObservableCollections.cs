﻿using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using NUnit.Framework;
using PropertyBinders.Tests.Annotations;

namespace PropertyBinders.Tests
{
    [TestFixture]
    public class Specs_For_Binding_ObservableCollections
    {
        private RootModel model;
        private RootViewModel viewModel;
        private PropertyChangedBinder<RootModel, RootViewModel> binder;

        [Test]
        public void It_Should_Be_Able_To_Construct_Property_Binder_For_Collections()
        {
            binder.BindObjects(model, viewModel);
            model.Items = new ObservableCollection<string>(new []{"New Name"});

            Assert.That(viewModel.Items[0], Is.EqualTo("ViewModel-New Name"));
        }

        [Test]
        public void It_Should_Be_Able_To_Sync_Additions_To_The_Underlying_Collection()
        {
            model.Items = new ObservableCollection<string>();
            binder.BindObjects(model, viewModel);
            
            model.Items.Add("Model1");

            CollectionAssert.Contains(viewModel.Items, "ViewModel-Model1");
        }

        [Test]
        public void It_Should_Be_Able_To_Sync_Removals_From_The_Underlying_Collection()
        {
            model.Items = new ObservableCollection<string>(){"1Model", "2Model"};
            binder.BindObjects(model, viewModel).Sync();

            model.Items.Remove("2Model");

            CollectionAssert.DoesNotContain(viewModel.Items, "ViewModel-2Model");
        }

        [Test]
        public void It_Should_Be_Able_To_Sync_Clears_From_The_Underlying_Collection()
        {
            model.Items = new ObservableCollection<string>() { "1Model", "2Model" };
            binder.BindObjects(model, viewModel).Sync();

            model.Items.Clear();

            CollectionAssert.IsEmpty(viewModel.Items);
        }

        [SetUp]
        public void CreateModels()
        {
            model = new RootModel();
            viewModel = new RootViewModel();
        }

        [TestFixtureSetUp]
        public void Setup()
        {
            binder = Binder
                .BindOnPropertyChanged<RootModel, RootViewModel>().BindCollection<string, string>(m => m.Items)
                    .Converter(m => "ViewModel-" + m)
                    .Finder((t, m) => t.Items.First(vm => vm.EndsWith(m)))
                    .To(vm => vm.Items);
        }

        public class RootModel : ICollectionPropertyChanged
        {
            private ObservableCollection<string> _items;

            public ObservableCollection<string> Items
            {
                get { return _items; }
                set
                {
                    if (Equals(value, _items)) return;
                    
                    if(_items != null){ _items.CollectionChanged -= ItemsOnCollectionChanged; }
                    
                    _items = value;

                    if (_items != null) { _items.CollectionChanged += ItemsOnCollectionChanged; }

                    OnPropertyChanged();
                }
            }

            private void ItemsOnCollectionChanged(object sender, NotifyCollectionChangedEventArgs args)
            {            
                CollectionPropertyChanged(this, new CollectionPropertyChangedEventArgs("Items", args));
            }

            public event PropertyChangedEventHandler PropertyChanged;

            [NotifyPropertyChangedInvocator]
            protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
            {
                var handler = PropertyChanged;
                if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
            }

            public event CollectionPropertyChangedEventHandler CollectionPropertyChanged = (a, b) => { };
        }



        public class RootViewModel : INotifyPropertyChanged
        {
            private ObservableCollection<string> _items = new ObservableCollection<string>();

            public ObservableCollection<string> Items
            {
                get { return _items; }
                set
                {
                    if (Equals(value, _items)) return;
                    _items = value;
                    OnPropertyChanged();
                }
            }

            public event PropertyChangedEventHandler PropertyChanged;

            [NotifyPropertyChangedInvocator]
            protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
            {
                var handler = PropertyChanged;
                if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}
